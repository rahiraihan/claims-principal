﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ClaimsPrincipalPractice.Controllers
{
    public class CustomController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        protected async Task<int> GetUserIdFromClaim()
        {
            var principal = this.HttpContext.User;
            if (principal == null) throw new Exception("User not found");
            var claims = principal.Claims.ToList();
            var userId = claims.FirstOrDefault(c => c.Type == "UserId")?.Value;
            return await System.Threading.Tasks.Task.FromResult(Convert.ToInt32(userId));
        }
        protected async Task<string> GetUserNameFromClaim()
        {
            var principal = this.HttpContext.User;
            if (principal == null) throw new Exception("User not found");
            var claims = principal.Claims.ToList();
            var username = claims.FirstOrDefault(c => c.Type == "UserName")?.Value;
            return await System.Threading.Tasks.Task.FromResult(username);
        }
        protected async Task<string> GetUserFullNameFromClaim()
        {
            var principal = HttpContext.User;
            if (principal == null) throw new Exception("User not found");
            var claims = principal.Claims.ToList();
            var userFullName = claims.FirstOrDefault(c => c.Type == "UserFullName")?.Value;
            return await System.Threading.Tasks.Task.FromResult(userFullName);
        }
       
    }
}