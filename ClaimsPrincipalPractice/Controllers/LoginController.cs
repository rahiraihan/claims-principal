﻿using System.Threading.Tasks;
using Claims.Data.Entities;
using Claims.Data.ViewModels;
using Claims.Service.ClaimsService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ClaimsPrincipalPractice.Controllers
{
    public class LoginController : CustomController
    {
        private readonly IUserService _service;
        private readonly DataContext _context;

        public LoginController(IUserService service, DataContext context)
        {
            _service = service;
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                var userid = await GetUserIdFromClaim();
                var get = await _service.GetAllUser(userid);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                var userdetails = await _context.Users
                .SingleOrDefaultAsync(m => m.UserName == login.UserName && m.Password == login.Password);
                if (userdetails == null)
                {
                    ModelState.AddModelError("Password", "Invalid login attempt.");
                    return View("Index");
                }
                HttpContext.Session.SetString("UserName", userdetails.UserName);

            }
            else
            {
                return View("Index");
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> Registar(RegistrationViewModel model)
        {

            if (ModelState.IsValid)
            {
                Users user = new Users
                {
                    UserName = model.UserName,
                    Password = model.Password,
                    UserGender = model.UserGender,
                    UserRole = model.UserRole,
                    

                };
                _context.Add(user);
                await _context.SaveChangesAsync();

            }
            else
            {
                return View("Registration");
            }
            return RedirectToAction("Index", "Registration");
        }

        public IActionResult Registration()
        {
            ViewData["Message"] = "Registration Page";

            return View();
        }

    }
}