﻿using System;
using System.Collections.Generic;

namespace Claims.Data.Entities
{
    public partial class Users
    {
        public Users()
        {
            BrandsAdd = new HashSet<BrandsAdd>();
            Customer = new HashSet<Customer>();
            ProductAdd = new HashSet<ProductAdd>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserGender { get; set; }
        public string UserRole { get; set; }

        public virtual ICollection<BrandsAdd> BrandsAdd { get; set; }
        public virtual ICollection<Customer> Customer { get; set; }
        public virtual ICollection<ProductAdd> ProductAdd { get; set; }
    }
}
