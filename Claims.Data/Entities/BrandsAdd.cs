﻿using System;
using System.Collections.Generic;

namespace Claims.Data.Entities
{
    public partial class BrandsAdd
    {
        public int BrandsAddId { get; set; }
        public string BrandsName { get; set; }
        public string Manufacturer { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int? UserId { get; set; }

        public virtual Users User { get; set; }
    }
}
