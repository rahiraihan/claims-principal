﻿using System;
using System.Collections.Generic;

namespace Claims.Data.Entities
{
    public partial class Customer
    {
        public int CustomerId { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerFullName { get; set; }
        public int? UserId { get; set; }

        public virtual Users User { get; set; }
    }
}
