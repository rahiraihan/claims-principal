﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Claims.Data.ViewModels
{
    public class RegistrationViewModel
    {
        
        [StringLength(15, MinimumLength = 3)]
        //[StringLength(15, ErrorMessage = "Name length can't be more than 15.")]
        public string UserName { get; set; }

        public string Password { get; set; }

        
        [NotMapped] // Does not effect with your database
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        
        public string UserRole { get; set; }

        
        


        public string UserGender { get; set; }
    }
}
