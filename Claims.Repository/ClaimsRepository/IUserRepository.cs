﻿using Claims.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Claims.Repository.ClaimsRepository
{
    public interface IUserRepository
    {
        Task<List<Users>> GetAllUser(int userid);
        Task<Users> CreateNewUser(Users user);
    }

    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;

        public UserRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<Users> CreateNewUser(Users user)
        {
            try
            {
                Users userTable = new Users();
                _context.Users.Add(user);
                await _context.SaveChangesAsync();
                return userTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Users>> GetAllUser(int userid)
        {
            try
            {
                var get = from c in _context.Users
                          orderby c.UserId 
                          select c;

                return await get.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
