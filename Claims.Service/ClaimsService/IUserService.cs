﻿using Claims.Data.Entities;
using Claims.Repository.ClaimsRepository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Claims.Service.ClaimsService
{
    public interface IUserService
    {
        Task<List<Users>> GetAllUser(int userid);
        Task<Users> CreateNewUser(Users user);
    }
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;

        public UserService(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<Users> CreateNewUser(Users user)
        {
            try
            {
                var createUser = await _repository.CreateNewUser(user);

                return createUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<List<Users>> GetAllUser(int userid)
        {
            try
            {
                var getUser = await _repository.GetAllUser(userid);

                return getUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
